#!/usr/bin/env bash

if mysql -u vagrant -ppassword -e 'use mysql;'  ; then
	sudo mysqldump -u vagrant -ppassword --all-databases > /vagrant/dump.sql
fi
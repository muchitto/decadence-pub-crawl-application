var gulp = require('gulp');

var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

gulp.task('js-dependencies', function () {
    gulp.src(['./node_modules/lodash/lodash.min.js',
                './node_modules/angular/angular.min.js',
                './node_modules/angular-route/angular-route.min.js',
                './node_modules/angular-resource/angular-resource.min.js',
                './node_modules/angular-simple-logger/dist/angular-simple-logger.min.js',
                './node_modules/angular-google-maps/dist/angular-google-maps.min.js'])
        .pipe(concat('dependencies.js'))
        .pipe(gulp.dest('./public/js/dependencies/'));
});

gulp.task('sass', function () {
    return gulp.src('./sass/**/*.scss')
                .pipe(sass().on('error', sass.logError))
                .pipe(rename('style.css'))
                .pipe(gulp.dest('./public/css/'));
});

gulp.task('js', function () {
});

gulp.task('default', ['js-dependencies', 'sass'], function () {
    gulp.watch('./sass/**/*.scss', ['sass']);
});
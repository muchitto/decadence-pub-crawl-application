var mainApp = angular.module('mainApp', ['ngRoute', 'ngResource', 'uiGmapgoogle-maps']);

mainApp.factory('User', function ($resource) {
    var user = {};

    user.loggedin = false;
    user.myroutes = {};

    user.refreshUserState = function (callback) {
        $resource('/api/userstate').get(function (data) {
            user.loggedin = data.success;

            if(data.success) {
                user.myroutes = data.routes;
            }

            if(typeof(callback) == 'function')
                callback(data.success);
        });
    }

    user.login = function (info, callback) {
        $resource('/api/login/:username/:password').get(info, function (data) {
            user.refreshUserState(callback);
        });
    }

    user.refreshUserState();

    return user;
});

mainApp.factory('Location', function ($http) {
    var service = {};

    service.getLocationDataByAddress = function (address, callback) {
        $http.get('http://maps.google.com/maps/api/geocode/json?address=' + address + '&sensor=false').then(function (result) {
            if (result.statusText == "OK") {

                if (result.data.results.length > 0) {
                    var location = result.data.results[0].geometry.location;

                    callback(location);
                }

            }
        });
    }

    return service;
});

mainApp.controller('MainController', function ($scope, $window, User) {
    $scope.loggedin = function () {
        return User.loggedin;
    }
});

mainApp.controller('MapController', function ($scope, $interval, $window, Location, $http) {

    $scope.map = {
        center: {
            latitude: 45,
            longitude: -73
        },
        zoom: 16,
        markers : [],
        options : {
            disableDoubleClickZoom : true
        },
        events : {
            dblclick : function(mapModel, eventName, originalEventArgs) {

                var marker = {
                    id : Date.now(),
                    coords : {
                        latitude : originalEventArgs[0].latLng.lat(),
                        longitude : originalEventArgs[0].latLng.lng()
                    }
                }
                $scope.map.markers.push(marker);
            }
        }
    };

    if($window.navigator.geolocation) {
        $window.navigator.geolocation.getCurrentPosition(function (position) {
            $scope.map.center.latitude = position.coords.latitude;
            $scope.map.center.longitude = position.coords.longitude;
        });
    }
});

mainApp.controller('MyRoutesController', function ($scope, $resource, User, $timeout) {
    User.refreshUserState(function () {
        $scope.routes = User.myroutes;
    });
});

mainApp.controller('LoginController', function ($scope, $location, $http, User) {
    $scope.login = function (info) {
        User.login(info, function (data) {
            if(data)
                $location.path('/myroutes');
        });
    }
});

mainApp.controller('RouteController', function ($scope, $resource, $routeParams) {
    $scope.route = {};

    $scope.map = {};

    $resource('/api/routeinfo/:id').get({id : $routeParams.routeid}, function (data) {
        $scope.route = data.info;
        $scope.map = {
            center : {
                latitude : data.info.locations[0].lat,
                longitude : data.info.locations[0].lng,
            },
            zoom: 16,
            options : {
                disableDoubleClickZoom : true
            },
            markers : [],
            polylines_path : []
        }

        for(l in data.info.locations) {
            var location = data.info.locations[l];

            $scope.map.markers.push({
                id : location.id,
                name : location.name,
                windowOptions : {
                  visible : false
                },
                coords : {
                    latitude : location.lat,
                    longitude : location.lng
                }
            });

            $scope.map.polylines_path.push({
                latitude : location.lat,
                longitude : location.lng
            });
        }
    });

    $scope.onClick = function(marker) {
        for(m in $scope.map.markers) {
            $scope.map.markers[m].windowOptions.visible = false;
        }

        marker.windowOptions.visible = true;
    };

    $scope.closeClick = function(marker) {
        marker.windowOptions.visible = false;
    };

});

mainApp.controller('RouteEditorController', function ($scope, $location, $window, $resource, $routeParams, User, Location) {
    $scope.isNew = true;

    if($routeParams.routeid) {
        $scope.isNew = false;

        $resource('/api/routeinfo/:id').get({id : $routeParams.routeid}, function (data) {
            if(data.success) {
                $scope.route = {
                    id : data.info.id,
                    name : data.info.name,
                    locations : []
                }

                for(l in data.info.locations) {
                    var location = data.info.locations[l];

                    var loc_data = {};

                    loc_data.vicinity = location.vicinity;
                    loc_data.name = location.name;
                    loc_data.id = location.location_id;
                    loc_data.data = location;

                    $scope.route.locations.push(loc_data);
                }

                $scope.refreshPolyLine();
            }
        });
    } else {
        $scope.route = {
            id : -1,
            name : "",
            locations : []
        };
    }

    $scope.isLoadingPlaces = false;

    $scope.map = {
        center: {
            latitude: 60.183062499999984,
            longitude: 24.953031471163957
        },
        zoom: 16,
        markers : [],
        options : {
            disableDoubleClickZoom : true
        },
        polylines_path : [],
        events : {
            idle : function (mapModel, eventName, originalEventArgs) {
                $scope.isLoadingPlaces = true;

                $resource('/api/getplaces/:lat/:lng').get({lat : mapModel.center.lat(), lng : mapModel.center.lng()}, function (data) {
                    if(data.success) {
                        for (d in data.results) {
                            var result = data.results[d];

                            if ($scope.isMarkerIn(result.id))
                                continue;

                            var marker = {
                                id: result.id,
                                name : result.name,
                                vicinity : result.vicinity,
                                data : result,
                                coords: {
                                    latitude: result.geometry.location.lat,
                                    longitude: result.geometry.location.lng
                                },
                                windowOptions : {
                                    visible : false
                                }
                            };
                            $scope.map.markers.push(marker);
                        }
                    }

                    $scope.isLoadingPlaces = false;
                });
            }
        }
    };

    $scope.refreshPolyLine = function () {
        $scope.map.polylines_path = [];
        for(l in $scope.route.locations) {

            var linepoint = {
                latitude: $scope.route.locations[l].data.lat,
                longitude: $scope.route.locations[l].data.lng
            }

            $scope.map.polylines_path.push(linepoint);

        }
    }

    $scope.windowOptions = {
        visible: false
    };

    $scope.removeLocation = function (index) {
        $scope.route.locations.splice(index, 1);

        $scope.refreshPolyLine();
    }

    $scope.onClick = function(marker) {
        for(m in $scope.map.markers) {
            $scope.map.markers[m].windowOptions.visible = false;
        }

        marker.windowOptions.visible = true;
    };

    $scope.closeClick = function(marker) {
        marker.windowOptions.visible = false;
    };

    $scope.save = function () {
        $resource('/api/saveroute').save($scope.route, function (data) {
            $location.path('/route/' + data.id);
        });
    }

    $scope.addToRoute = function (place_id) {
        if($scope.route.locations.length) {
            if ($scope.route.locations[$scope.route.locations.length - 1].place_id == place_id) {
                return;
            }
        }

        for(m in $scope.map.markers) {
            var marker = $scope.map.markers[m];

            if(marker.id == place_id) {

                var route_location_data = {
                    id : marker.id,
                    name : marker.name,
                    vicinity : marker.data.vicinity,
                    data : {
                        lat : marker.coords.latitude,
                        lng : marker.coords.longitude
                    }
                }

                $scope.route.locations.push(route_location_data);

                break;
            }
        }

        $scope.refreshPolyLine();
    }

    if($window.navigator.geolocation) {
        $window.navigator.geolocation.getCurrentPosition(function (position) {
            $scope.map.center.latitude = position.coords.latitude;
            $scope.map.center.longitude = position.coords.longitude;
        });
    }

    $scope.isMarkerIn = function (id) {
        for (m in $scope.map.markers) {
            if(id == $scope.map.markers[m].id)
                return true;
        }
        return false;
    }

    $scope.addLocation = function () {
        $scope.route.locations.push({
            vicinity : 'Porthaninkatu 11 D 58'
        });
    }

    $scope.changeLocation = function (address) {
        Location.getLocationDataByAddress(address, function (location) {
            $scope.map.center.latitude = location.lat;
            $scope.map.center.longitude = location.lng;
        });
    }
});

mainApp.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/route/:routeid', {
        templateUrl : 'partials/route.html',
        controller : 'RouteController'
    }).when('/routeeditor/:routeid?', {
        templateUrl : 'partials/routeeditor.html',
        controller : 'RouteEditorController'
    }).when('/myroutes', {
        templateUrl : 'partials/myroutes.html',
        controller : 'MyRoutesController'
    }).when('/login', {
        templateUrl : 'partials/login.html',
        controller : 'LoginController'
    }).when('/logout', {
        resolve : {
            app : function ($resource, $location, User) {
                $resource('/api/logout').get(function (data) {
                    User.refreshUserState();
                    $location.path('/');
                });
            }
        }
    });

    // use the HTML5 History API
    $locationProvider.html5Mode(true);

}).run(function ($rootScope, $location, $resource, User) {

    User.refreshUserState(function (success) {
        if(!success && $location.url().indexOf("route/") == -1)
            $location.path('/login');
    });
});
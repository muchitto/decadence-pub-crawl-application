<?php

require '../../vendor/autoload.php';

use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Phalcon\Session\Adapter\Files as Session;
use Phalcon\Config\Adapter\Ini as Config;
use Phalcon\Events\Manager as EventsManager;

$environment = "development";

$loader = new \Phalcon\Loader();

$loader->registerDirs(
	array(
		__DIR__ . '/models/'
	)
)->register();

$di = new FactoryDefault();

$di->setShared('session', function () {
	$session = new Session();
	$session->start();
	return $session;
});

$config = new Config ('../../configs/' . $environment . '.ini');

$di->set('config', function () use ($config) {
	return $config;
});

$di->set('db', function () use ($config)  {

	return new PdoMysql(
		array(
			"host"     => $config->database->host,
			"username" => $config->database->username,
			"password" => $config->database->password,
			"dbname"   => $config->database->dbname
		)
	);
});

$eventsManager = new EventsManager();

$eventsManager->attach('micro', function ($event, $app) use ($di) {
	if ($event->getType() == 'beforeExecuteRoute') {
		$name = $app->getRouter()->getMatchedRoute ()->getName();

		$user = Users::findFirst(array(
			'conditions' => 'sessionid = ?1',
			'bind' => array(1 => $app->session->get('sessionid'))
		));

		$di->set('auth', function () use ($user) {
			return $user;
		});

		if(!$user) {
			if (!in_array($name, array('login', 'routeinfo'))) {

				echo json_encode(array('success' => false));
				return false;
			}
		}
	}
});


$app = new Micro($di);
$app->setEventsManager($eventsManager);

$app->get('/', function () {

});

$app->get('/login/{username}/{password}', function ($username, $password) use ($app) {
	$user = Users::findFirst(array(
		'conditions' => 'username = ?1 AND password = ?2',
		'bind' => array(1 => $username, 2 => md5($password . $app->config['users']['salt']))
	));

	if($user) {
		$app->session->set('sessionid', sha1(microtime()));

		$user->sessionid = $app->session->get('sessionid');
		$user->save();

		echo json_encode(array('success' => true));
	} else {
		echo json_encode(array('success' => false));
	}
})->setName("login");

$app->get('/userstate', function () use ($app) {

	$user_routes = $app->auth->getRoutes();

	if(count($user_routes)) {
		$routes = array();
	}

	echo json_encode(array('success' => true, 'routes' => $user_routes->toArray()));

})->setName("userstate");

$app->get('/logout', function () use ($app) {
	$app->session->set('sessionid', '');

	echo json_encode(array('success' => true));
});

$app->post('/saveroute', function () use ($app) {
	$data = $app->request->getJsonRawBody();

	foreach($data->locations as $location_data) {
		$location = Locations::findFirst(array(
			'conditions' => 'location_id = ?1',
			'bind' => array(1 => $location_data->id)
		));

		if(!$location || $location->last_updated + 1800 < time()) {
			if(!$location) {
				$location = new Locations();
			}

			$location->name = $location_data->name;

			$location->lat = $location_data->data->lat;
			$location->lng = $location_data->data->lng;
			$location->location_id = $location_data->id;
			$location->last_updated = time();
			$location->vicinity = $location_data->vicinity;

			$location->save();
		}
	}

	if($data->id == -1) {
		$route = new Routes ();
		$route->owner_id = $app->auth->id;
		$route->name = $data->name;
		$route->save();

	} else {
		$route = Routes::findFirst($data->id);

		foreach($route->Locations as $location) {
			$ltr = LocationsToRoutes::findFirst(array(
				'conditions' => 'location_id = ?1 AND route_id = ?2',
				'bind' => array(1 => $location->id, 2 => $route->id)
			));

			if($ltr)
				$ltr->delete();
		}
	}

	$order = 0;
	foreach($data->locations as $location_data) {
		$location = Locations::findFirst(array(
			'conditions' => 'location_id = ?1',
			'bind' => array(1 => $location_data->id)
		));

		$route_location = new LocationsToRoutes();
		$route_location->route_id = $route->id;
		$route_location->location_id = $location->id;
		$route_location->order = $order;

		$route_location->save();
		$order++;
	}

	echo json_encode(array('success' => true, 'id' => $route->id));

})->setName("saveRoute");

$app->get('/routeinfo/{routeid}', function ($routeid) use ($app) {

	$route = Routes::findFirst(array(
		'conditions' => "id = ?1",
		'bind' => array(1 => $routeid)
	));

	if($route) {
		$route_data = $route->toArray();
		$route_data['locations'] = $route->Locations->toArray();

		echo json_encode(array('success' => true, 'info' => $route_data));
	} else {
		echo json_encode(array('success' => false));
	}

})->setName("routeinfo");

$app->get('/getplaces/{lat}/{lng}', function ($lat, $lng) use ($app) {

	if($app->config->google->use_test_data) {
		echo file_get_contents($app->config->google->test_data);
		return;
	} else {
		$google_places = new joshtronic\GooglePlaces($app->config->google->places_api_key);

		$google_places->location = array($lat, $lng);
		$google_places->radius = 1000;
		$google_places->types = 'bar';

		$results_complete = array();
		$run = true;
		$errors = array();
		while ($run) {
			$results = $google_places->nearbySearch();

			if (isset($results['error_message'])) {
				$errors = $results['error_message'];
				break;
			}

			foreach ($results['results'] as $result) {
				$results_complete[] = $result;
			}

			if (isset($results['next_page_token'])) {
				$google_places->pagetoken = $results['next_page_token'];
			} else {
				$run = false;
			}
		}

		if(count($results['results']) > 0) {
			echo json_encode(array('success' => true, 'results' => $results_complete));
		} else {
			echo json_encode(array('success' => false, 'errors' => $errors));
		}
	}

})->setName("getPlaces");

$app->handle();
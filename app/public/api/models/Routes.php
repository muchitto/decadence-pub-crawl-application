<?php

use Phalcon\Mvc\Model;

class Routes extends Model
{
	public $id;
	public $name;
	public $owner_id;

	public function initialize()
	{
		$this->setSource("routes");
		$this->hasManyToMany ("id", "LocationsToRoutes", "route_id", "location_id", "Locations", "id");
	}
}
<?php

use Phalcon\Mvc\Model;

class Users extends Model {
	public $id;
	public $username;
	public $password;
	public $sessionid;

	public function initialize() {
		$this->setSource("users");
		$this->hasMany ("id", "Routes", "owner_id");
	}
}
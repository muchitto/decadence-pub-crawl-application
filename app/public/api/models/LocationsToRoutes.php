<?php

use Phalcon\Mvc\Model;

class LocationsToRoutes extends Model
{
	public $id;
	public $order;
	public $route_id;
	public $location_id;
	public $visited;

	public function initialize () {
		$this->setSource("locations_to_routes");
	}
}
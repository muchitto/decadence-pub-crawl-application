<?php

use Phalcon\Mvc\Model;

class Locations extends Model
{
	public $id;
	public $vicinity;
	public $name;
	public $lat;
	public $lng;
	public $location_id;

	public function initialize()
	{
		$this->setSource('locations');
	}
}

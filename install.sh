#!/usr/bin/env bash

sudo apt-get update

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

sudo apt-get install -y software-properties-common python-software-properties

sudo apt-add-repository -y ppa:phalcon/stable
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
sudo add-apt-repository 'deb http://ftp.kaist.ac.kr/mariadb/repo/10.0/ubuntu trusty main'
sudo curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -

sudo apt-get update

#sudo rm /usr/bin/python
#sudo ln -s /usr/bin/python3.4 /usr/bin/python

sudo apt-get install -y mariadb-server
sudo apt-get install -y build-essential
sudo apt-get install -y nginx
sudo apt-get install -y nodejs
sudo apt-get install -y git

#PHP / PHALCON

sudo apt-get install -y php5-common php5-cli php5-fpm php5-mysql php5-phalcon php5-curl

#PYTHON STUFF
#sudo apt-get install -y python-pip python-dev
#sudo pip install --upgrade pip
#sudo pip install --upgrade virtualenv

sudo npm install npm@latest

npm install --global gulp
npm install --global gulp-cli

sudo gem install bourbon
sudo gem install neat
sudo gem install bitters

sudo curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

sudo ln -s /srv/app/ /home/vagrant/app

sudo cp /vagrant/nginx-configuration /etc/nginx/sites-available/application
sudo ln -s /etc/nginx/sites-available/application /etc/nginx/sites-enabled/application
sudo rm /etc/nginx/sites-enabled/default

sudo sed -i "s/^display_errors = Off/display_errors = On/" /etc/php5/fpm/php.ini
sudo sed -i "s/^display_startup_errors = Off/display_startup_errors = On/" /etc/php5/fpm/php.ini

sudo sed -i "s/^bind-address/#bind-address/" /etc/mysql/my.cnf
sudo mysql -u root -proot -e "CREATE USER 'vagrant'@'%' IDENTIFIED BY 'password'"
sudo mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'%'; FLUSH PRIVILEGES;"

sudo service nginx restart
sudo service php5-fpm restart
sudo service mysql restart

sudo updatedb

if [ -e "/vagrant/dump.sql" ]; then
	sudo mysql -u vagrant -ppassword < /vagrant/dump.sql
fi